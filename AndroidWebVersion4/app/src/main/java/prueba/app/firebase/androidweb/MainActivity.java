package prueba.app.firebase.androidweb;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import prueba.app.firebase.androidweb.Objetos.Contactos;
import prueba.app.firebase.androidweb.Objetos.ProcesosPHP;
import prueba.app.firebase.androidweb.Objetos.Device;


public class MainActivity extends AppCompatActivity  implements View.OnClickListener
{
    private Button btnGuardar;
    private Button btnListar;
    private Button btnLimpiar;
    private TextView txtNombre;
    private TextView txtDireccion;
    private TextView txtTelefono1;
    private TextView txtTelefono2;
    private TextView txtNotas;
    private CheckBox cbkFavorite;
    private Contactos savedContacto;

    private String oldNombre;
    private String oldDirec;
    private String oldTel1;
    private String oldTel2;
    private String oldNotas;
    private int oldFav = 0;
    ProcesosPHP php;
    private int id;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initComponents();
        setEvents();
        getContacto();
    }
    private void getContacto () {
        try {
            Intent intent = getIntent();
            Bundle data = intent.getExtras();
            Contactos contacto = (Contactos) data.getSerializable("contacto");
            if (contacto != null) {
                txtNombre.setText(contacto.getNombre());
                oldNombre = txtNombre.getText().toString();
                txtTelefono1.setText(contacto.getTelefono1());
                oldTel1 = txtTelefono1.getText().toString();
                txtTelefono2.setText(contacto.getTelefono2());
                oldTel2 = txtTelefono2.getText().toString();
                txtDireccion.setText(contacto.getDireccion());
                oldDirec = txtDireccion.getText().toString();
                txtNotas.setText(contacto.getNotas());
                oldNotas = txtNotas.getText().toString();
                if(contacto.getFavorite()>0)
                {
                    cbkFavorite.setChecked(true);
                    oldFav = 1;
                }
                this.savedContacto = contacto;
            }
        } catch (Exception ex) {

        }
    }
    public void initComponents()
    {
        this.php = new ProcesosPHP();
        php.setContext(this);
        this.txtNombre = findViewById(R.id.edtNombre);
        this.txtTelefono1 = findViewById(R.id.edtTelefono1);
        this.txtTelefono2 = findViewById(R.id.edtTelefono2);
        this.txtDireccion = findViewById(R.id.edtDireccion);
        this.txtNotas = findViewById(R.id.edtNotas);
        this.cbkFavorite = findViewById(R.id.cbxFavorito);
        this.btnGuardar = findViewById(R.id.btnGuardar);
        this.btnListar = findViewById(R.id.btnListar);
        this.btnLimpiar = findViewById(R.id.btnLimpiar);
        savedContacto = null;
    }
    public void setEvents()
    {
        this.btnGuardar.setOnClickListener(this);
        this.btnListar.setOnClickListener(this);
        this.btnLimpiar.setOnClickListener(this);
    }

    @Override
    public void onClick(View view)
    {
        if(isNetworkAvailable()){
            switch (view.getId()) {
                case R.id.btnGuardar:
                    boolean completo = true;
                    if(txtNombre.getText().toString().equals(""))
                    {
                        txtNombre.setError("Introduce el Nombre");
                        completo=false;
                    }
                    if(txtTelefono1.getText().toString().equals(""))
                    {
                        txtTelefono1.setError("Introduce el Telefono Principal");
                        completo=false;
                    }
                    if(txtDireccion.getText().toString().equals(""))
                    {
                        txtDireccion.setError("Introduce la Direccion");
                        completo=false;
                    }
                    if (completo)
                    {
                        Toast.makeText(getApplicationContext(),Device.getSecureId(getApplicationContext()),Toast.LENGTH_SHORT);
                        if(savedContacto == null)
                        {
                            dialogBoxGuardar();
                        }
                        else
                        {
                            dialogBoxActualizar();
                        }

                    }
                    break;
                case R.id.btnLimpiar:
                    limpiar();
                    break;
                case R.id.btnListar:
                    Intent i= new Intent(MainActivity.this,ListaActivity.class);
                    limpiar();
                    startActivityForResult(i,0);
                    break;
            }
        }else{
            Toast.makeText(getApplicationContext(), "Se necesita tener conexion a internet", Toast.LENGTH_SHORT).show();
        }
    }
    public boolean isNetworkAvailable()
    {
        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        return ni != null && ni.isConnected();
    }
    public void limpiar(){
        savedContacto = null;
        txtNombre.setText("");
        txtTelefono1.setText("");
        txtTelefono2.setText("");
        txtNotas.setText("");
        txtDireccion.setText("");
        cbkFavorite.setChecked(false);
        txtNombre.findFocus();
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent intent)
    {
        super.onActivityResult(requestCode,resultCode,intent);
        if(intent != null){
            Bundle oBundle = intent.getExtras();
            if(Activity.RESULT_OK == resultCode)
            {
                Contactos contacto = (Contactos)oBundle.getSerializable("contacto");
                savedContacto = contacto;
                id = contacto.get_ID();
                txtNombre.setText(contacto.getNombre());
                oldNombre = txtNombre.getText().toString();
                txtTelefono1.setText(contacto.getTelefono1());
                oldTel1 = txtTelefono1.getText().toString();
                txtTelefono2.setText(contacto.getTelefono2());
                oldTel2 = txtTelefono2.getText().toString();
                txtDireccion.setText(contacto.getDireccion());
                oldDirec = txtDireccion.getText().toString();
                txtNotas.setText(contacto.getNotas());
                oldNotas = txtNotas.getText().toString();
                if(contacto.getFavorite()>0)
                {
                    cbkFavorite.setChecked(true);
                    oldFav = 1;
                }
            }else{
                limpiar();
            }
        }
    }
    public int cambios()
    {
        int vf = 0;
        int var = 1;
        if(cbkFavorite.isChecked())
        {
            vf = 1;
        }
        Toast.makeText(getApplicationContext(),  txtNombre.getText().toString()+ " - " + oldNombre +" / " +
                        txtTelefono2.getText().toString()+" - " + oldTel2 +" / " +
                        txtNotas.getText().toString()+" - " + oldNotas + " / " +
                        txtTelefono1.getText().toString()+ " - " +oldTel1 +" / " +
                        txtDireccion.getText().toString()+" - " + oldDirec +" / " +
                        vf +" - " + oldFav, Toast.LENGTH_SHORT).show();
        if(txtNombre.getText().toString().compareTo(oldNombre) != 0 ||
                txtTelefono1.getText().toString().compareTo(oldTel1) != 0 ||
                txtTelefono2.getText().toString().compareTo(oldTel2) != 0 ||
                txtDireccion.getText().toString().compareTo(oldDirec) != 0 ||
                txtNotas.getText().toString().compareTo(oldNotas) != 0 ||
                vf != oldFav)
        {
            var = 1;
        }

/*
        if(txtNombre.getText().toString() != oldNombre)
        {
            var = 1;
            Toast.makeText(getApplicationContext(), "nombre:  " + oldNombre + txtNombre.getText().toString(), Toast.LENGTH_SHORT).show();

        }
        if(txtNotas.getText().toString() != oldNotas)
        {
            var = 1;
            Toast.makeText(getApplicationContext(), "notas:  " + oldNotas + txtNotas.getText().toString(), Toast.LENGTH_SHORT).show();

        }
        if(txtDireccion.getText().toString() != oldDirec)
        {
            var = 1;
            Toast.makeText(getApplicationContext(), "dire:  " + oldDirec + txtDireccion.getText().toString(), Toast.LENGTH_SHORT).show();

        }
        if(txtTelefono1.getText().toString() != oldTel1)
        {
            var = 1;
            Toast.makeText(getApplicationContext(), "tel1:  " + oldTel1 + txtTelefono1.getText().toString(), Toast.LENGTH_SHORT).show();

        }
        if(txtTelefono2.getText().toString() != oldTel2)
        {
            var = 1;
            Toast.makeText(getApplicationContext(), "tel2:  " + oldTel2 + txtTelefono2.getText().toString(), Toast.LENGTH_SHORT).show();

        }
        if(vf != oldFav)
        {
            var = 1;
            Toast.makeText(getApplicationContext(), "Fav:  " + oldFav + vf, Toast.LENGTH_SHORT).show();

        }
*/
        return var;
    }

    public boolean noHayCambios () {
        return !txtNombre.getText().toString().equals(savedContacto.getNombre()) ||
                !txtTelefono1.getText().toString().equals(savedContacto.getTelefono1()) ||
                !txtTelefono2.getText().toString().equals(savedContacto.getTelefono2()) ||
                !txtNotas.getText().toString().equals(savedContacto.getNotas()) ||
                !txtDireccion.getText().toString().equals(savedContacto.getDireccion()) ||
                (savedContacto.getFavorite() == 1) != cbkFavorite.isChecked();
    }
    public void dialogBoxActualizar()
    {
        if(noHayCambios())
        {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setCancelable(false);
            alertDialogBuilder.setMessage("¿Seguro que desea actualizar?");
            alertDialogBuilder.setPositiveButton("Si",
                    new DialogInterface.OnClickListener()
                    {

                        @Override
                        public void onClick(DialogInterface arg0, int arg1)
                        {
                            Toast.makeText(getApplicationContext(), "entro", Toast.LENGTH_SHORT).show();
                            Contactos nContacto = new Contactos();
                            nContacto.setNombre(txtNombre.getText().toString());
                            nContacto.setTelefono1(txtTelefono1.getText().toString());
                            nContacto.setTelefono2(txtTelefono2.getText().toString());
                            nContacto.setDireccion(txtDireccion.getText().toString());
                            nContacto.setNotas(txtNotas.getText().toString());
                            nContacto.setFavorite(cbkFavorite.isChecked() ? 1 : 0);
                            nContacto.setIdMovil(Device.getSecureId(MainActivity.this));
                            if(savedContacto == null)
                            {
                                php.insertarContactoWebService(nContacto);
                                Toast.makeText(getApplicationContext(),R.string.mensaje,
                                        Toast.LENGTH_SHORT).show();
                                limpiar();
                            }else{
                                php.actualizarContactoWebService(nContacto,id);
                                Toast.makeText(getApplicationContext(),R.string.mensajeedit,
                                        Toast.LENGTH_SHORT).show();
                                limpiar();
                            }
                            limpiar();

                        }
                    });

            alertDialogBuilder.setNegativeButton("Cancel",
                    new DialogInterface.OnClickListener()
                    {

                        @Override
                        public void onClick(DialogInterface arg0, int arg1)
                        {
                            Toast.makeText(getApplicationContext(),"bay", Toast.LENGTH_SHORT).show();
                        }
                    });

            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }
        else
        {
            Toast.makeText(getApplicationContext(),"No ha Cambiado ningun dato", Toast.LENGTH_SHORT).show();
        }

    }
    public void dialogBoxGuardar()
    {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setMessage("¿Seguro que desea guardar?");
        alertDialogBuilder.setPositiveButton("Si",
                new DialogInterface.OnClickListener()
                {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1)
                    {
                        Toast.makeText(getApplicationContext(), "entro", Toast.LENGTH_SHORT).show();
                        Contactos nContacto = new Contactos();
                        nContacto.setNombre(txtNombre.getText().toString());
                        nContacto.setTelefono1(txtTelefono1.getText().toString());
                        nContacto.setTelefono2(txtTelefono2.getText().toString());
                        nContacto.setDireccion(txtDireccion.getText().toString());
                        nContacto.setNotas(txtNotas.getText().toString());
                        nContacto.setFavorite(cbkFavorite.isChecked() ? 1 : 0);
                        nContacto.setIdMovil(Device.getSecureId(MainActivity.this));

                        if(savedContacto == null)
                        {
                            php.insertarContactoWebService(nContacto);
                            Toast.makeText(getApplicationContext(),R.string.mensaje,
                                    Toast.LENGTH_SHORT).show();
                            limpiar();
                        }else{
                            php.actualizarContactoWebService(nContacto,id);
                            Toast.makeText(getApplicationContext(),R.string.mensajeedit,
                                    Toast.LENGTH_SHORT).show();
                            limpiar();
                        }

                    }
                });

        alertDialogBuilder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener()
                {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1)
                    {
                        Toast.makeText(getApplicationContext(),"bay", Toast.LENGTH_SHORT).show();
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }






    /* DIALOG ABRIr

    */
}
