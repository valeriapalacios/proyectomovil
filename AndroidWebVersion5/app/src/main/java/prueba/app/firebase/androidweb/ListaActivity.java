package prueba.app.firebase.androidweb;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;

import prueba.app.firebase.androidweb.Objetos.Contactos;
import prueba.app.firebase.androidweb.Objetos.ProcesosPHP;
import prueba.app.firebase.androidweb.Objetos.Device;


public class ListaActivity extends AppCompatActivity implements Response.Listener<JSONObject>,Response.ErrorListener
{
    private int total = 0;
    private Button btnNuevo;
    private final Context context = this;
    private ProcesosPHP php = new ProcesosPHP();;
    private RequestQueue request;
    private JsonObjectRequest jsonObjectRequest;
    private ArrayList<Contactos> listaContactos;
    private String serverip = "http://2016030063.000webhostapp.com/WebService/";
    private Adapter adapter = null;

    private ListView lista;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);
        try {
            request = Volley.newRequestQueue(context);
            php.setContext(ListaActivity.this);
            lista = findViewById(R.id.list);
            listaContactos = new ArrayList<>();

            consultarTodosWebService();
            btnNuevo = (Button)findViewById(R.id.btnNuevo);
            btnNuevo.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(ListaActivity.this, MainActivity.class);
                    finish();
                    startActivity(intent);
                }
            });
        }
        catch (Exception e) {

        }
    }
    public void consultarTodosWebService()
    {
        try
        {
            String url = serverip +"wsConsultarTodos.php?idMovil="+ Device.getSecureId(this);
            jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
            request.add(jsonObjectRequest);
        }
        catch (Exception e)
        {
            Toast.makeText(getApplicationContext(),e.getMessage(),Toast.LENGTH_SHORT).show();
        }

    }
    @Override
    public void onErrorResponse(VolleyError error)
    {
        error.printStackTrace();
        Toast.makeText(getApplicationContext(),"Error en el volley",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResponse(JSONObject response)
    {
        Contactos contacto = null;
        try
        {
            int con = response.getInt("code");
            if(con != 0)
            {
                JSONArray json = response.optJSONArray("contactos");
                try
                {
                    for(int i=0;i<json.length();i++)
                    {
                        contacto = new Contactos();
                        JSONObject jsonObject = null;
                        jsonObject = json.getJSONObject(i);
                        contacto.set_ID(jsonObject.optInt("_ID"));
                        contacto.setNombre(jsonObject.optString("nombre"));
                        contacto.setTelefono1(jsonObject.optString("telefono1"));
                        contacto.setTelefono2(jsonObject.optString("telefono2"));
                        contacto.setDireccion(jsonObject.optString("direccion"));
                        contacto.setNotas(jsonObject.optString("notas"));
                        contacto.setFavorite(jsonObject.optInt("favorite"));
                        contacto.setIdMovil(jsonObject.optString("idMovil"));
                        listaContactos.add(contacto);

                    }
                    total = listaContactos.size();
                    adapter = new Adapter(ListaActivity.this, listaContactos);
                    lista.setAdapter(adapter);
                }
                catch (JSONException e)
                {
                    Toast.makeText(getApplicationContext(),e.getMessage(),Toast.LENGTH_SHORT).show();
                }
            }
            else
            {
                Toast.makeText(getApplicationContext(),"No hay ningun contacto registrado",Toast.LENGTH_SHORT).show();
            }
        }
        catch (JSONException e)
        {
            Toast.makeText(getApplicationContext(),e.getMessage(),Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public boolean onCreateOptionsMenu (Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_main, menu);
        MenuItem item = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) item.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                Toast.makeText(getApplicationContext(),s,Toast.LENGTH_SHORT).show();
                if (TextUtils.isEmpty(s))
                {
                    adapter.filter("");
                    lista.clearTextFilter();
                }
                else {
                    adapter.filter(s);
                }
                return true;
            }
        });
        return true;
    }
    public void setColor()
    {

    }
/*
    class MyArrayAdapter extends ArrayAdapter<Contactos>
    {
        Context context;
        int textViewRecursoId;
        ArrayList<Contactos> objects;
        public MyArrayAdapter(Context context, int textViewResourceId,ArrayList<Contactos> objects){
            super(context, textViewResourceId, objects);
            this.context = context;
            this.textViewRecursoId = textViewResourceId;
            this.objects = objects;
        }
        public View getView(final int position, View convertView, ViewGroup
                viewGroup)
        {
            int band = 0;
            LayoutInflater layoutInflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(this.textViewRecursoId, null);
            TextView lblNombre = (TextView)view.findViewById(R.id.lblNombreContacto);
            TextView lblTelefono = (TextView)view.findViewById(R.id.lblTelefonoContacto);
            RelativeLayout relativeLayout = (RelativeLayout)view.findViewById(R.id.rlC);
            Button btnModificar = (Button)view.findViewById(R.id.btnModificar);
            Button btnBorrar = (Button)view.findViewById(R.id.btnBorrar);
            if(objects.get(position).getFavorite()>0){
                lblNombre.setTextColor(Color.BLUE);
                lblTelefono.setTextColor(Color.BLUE);
            }else{
                lblNombre.setTextColor(Color.BLACK);
                lblTelefono.setTextColor(Color.BLACK);
            }
            lblNombre.setText(objects.get(position).getNombre());
            lblTelefono.setText(objects.get(position).getTelefono1());

            if(total%2==0)
            {
                relativeLayout.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.color1));
                total = total - 1;
            }
            else
            {
                relativeLayout.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.color2));
                total = total - 1;
            }
            btnBorrar.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {

                    dialogBoxBorrar(position,objects.get(position).get_ID());
                }
            });
            btnModificar.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v) {
                    Bundle oBundle = new Bundle();
                    oBundle.putSerializable("contacto", objects.get(position));
                    Intent i = new Intent();
                    i.putExtras(oBundle);
                    setResult(Activity.RESULT_OK, i);
                    finish();
                }
            });
            return view;
        }
        public void dialogBoxBorrar(final int position,final int id)
        {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ListaActivity.this);
            alertDialogBuilder.setCancelable(false);
            alertDialogBuilder.setMessage("¿Seguro que desea borrar este contacto?");
            alertDialogBuilder.setPositiveButton("Si",
                    new DialogInterface.OnClickListener()
                    {

                        @Override
                        public void onClick(DialogInterface arg0, int arg1)
                        {
                            php.setContext(context);
                            Log.i("id", String.valueOf(id));
                            php.borrarContactoWebService(id);
                            objects.remove(position);
                            notifyDataSetChanged();
                            Toast.makeText(getApplicationContext(), "Contacto eliminado con exito", Toast.LENGTH_SHORT).show();

                        }
                    });

            alertDialogBuilder.setNegativeButton("Cancel",
                    new DialogInterface.OnClickListener()
                    {

                        @Override
                        public void onClick(DialogInterface arg0, int arg1)
                        {
                            Toast.makeText(getApplicationContext(),"bay", Toast.LENGTH_SHORT).show();
                        }
                    });

            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }
    }*/
    public void borrar (int id) {
        php.borrarContactoWebService(id);
        total = listaContactos.size();
        adapter = new Adapter(ListaActivity.this, listaContactos);
        lista.setAdapter(adapter);
    }


}
