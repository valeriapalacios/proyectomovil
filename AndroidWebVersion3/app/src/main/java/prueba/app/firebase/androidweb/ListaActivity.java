package prueba.app.firebase.androidweb;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;

import prueba.app.firebase.androidweb.Objetos.Contactos;
import prueba.app.firebase.androidweb.Objetos.ProcesosPHP;
import prueba.app.firebase.androidweb.Objetos.Device;


public class ListaActivity extends ListActivity implements Response.Listener<JSONObject>,Response.ErrorListener
{
    private Button btnNuevo;
    private final Context context = this;
    private ProcesosPHP php = new ProcesosPHP();;
    private RequestQueue request;
    private JsonObjectRequest jsonObjectRequest;
    private ArrayList<Contactos> listaContactos;
    private String serverip = "http://2016030063.000webhostapp.com/WebService/";
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);
        listaContactos = new ArrayList<Contactos>();
        request = Volley.newRequestQueue(context);
        consultarTodosWebService();
        btnNuevo = (Button)findViewById(R.id.btnNuevo);
        btnNuevo.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                setResult(Activity.RESULT_CANCELED);
                finish();
            }
        });
    }
    public void consultarTodosWebService()
    {
        String url = serverip +"wsConsultarTodos.php?idMovil="+ Device.getSecureId(this);
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
        request.add(jsonObjectRequest);
        Toast.makeText(getApplicationContext(),jsonObjectRequest.toString(),Toast.LENGTH_SHORT);
    }
    @Override
    public void onErrorResponse(VolleyError error) {
    }

    @Override
    public void onResponse(JSONObject response) {
        Contactos contacto = null;
        JSONArray json = response.optJSONArray("contactos");
        try
        {
            Toast.makeText(getApplicationContext(),json.toString(),Toast.LENGTH_SHORT);
            for(int i=0;i<json.length();i++){
                contacto = new Contactos();
                JSONObject jsonObject = null;
                jsonObject = json.getJSONObject(i);
                contacto.set_ID(jsonObject.optInt("_ID"));
                contacto.setNombre(jsonObject.optString("nombre"));
                contacto.setTelefono1(jsonObject.optString("telefono1"));
                contacto.setTelefono2(jsonObject.optString("telefono2"));
                contacto.setDireccion(jsonObject.optString("direccion"));
                contacto.setNotas(jsonObject.optString("notas"));
                contacto.setFavorite(jsonObject.optInt("favorite"));
                contacto.setIdMovil(jsonObject.optString("idMovil"));
                listaContactos.add(contacto);

            }
            MyArrayAdapter adapter = new MyArrayAdapter(context,R.layout.layout_contacto,listaContactos);
            setListAdapter(adapter);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    class MyArrayAdapter extends ArrayAdapter<Contactos>
    {
        Context context;
        int textViewRecursoId;
        ArrayList<Contactos> objects;
        public MyArrayAdapter(Context context, int textViewResourceId,ArrayList<Contactos> objects){
            super(context, textViewResourceId, objects);
            this.context = context;
            this.textViewRecursoId = textViewResourceId;
            this.objects = objects;
        }
        public View getView(final int position, View convertView, ViewGroup
                viewGroup){
            LayoutInflater layoutInflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(this.textViewRecursoId, null);
            TextView lblNombre = (TextView)view.findViewById(R.id.lblNombreContacto);
            TextView lblTelefono = (TextView)view.findViewById(R.id.lblTelefonoContacto);
            Button btnModificar = (Button)view.findViewById(R.id.btnModificar);
            Button btnBorrar = (Button)view.findViewById(R.id.btnBorrar);
            if(objects.get(position).getFavorite()>0){
                lblNombre.setTextColor(Color.BLUE);
                lblTelefono.setTextColor(Color.BLUE);
            }else{
                lblNombre.setTextColor(Color.BLACK);
                lblTelefono.setTextColor(Color.BLACK);
            }
            lblNombre.setText(objects.get(position).getNombre());
            lblTelefono.setText(objects.get(position).getTelefono1());
            btnBorrar.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {

                    dialogBoxBorrar(position,objects.get(position).get_ID());
                }
            });
            btnModificar.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v) {
                    Bundle oBundle = new Bundle();
                    oBundle.putSerializable("contacto", objects.get(position));
                    Intent i = new Intent();
                    i.putExtras(oBundle);
                    setResult(Activity.RESULT_OK, i);
                    finish();
                }
            });
            return view;
        }
        public void dialogBoxBorrar(final int position,final int id)
        {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ListaActivity.this);
            alertDialogBuilder.setCancelable(false);
            alertDialogBuilder.setMessage("¿Seguro que desea borrar este contacto?");
            alertDialogBuilder.setPositiveButton("Si",
                    new DialogInterface.OnClickListener()
                    {

                        @Override
                        public void onClick(DialogInterface arg0, int arg1)
                        {
                            php.setContext(context);
                            Log.i("id", String.valueOf(id));
                            php.borrarContactoWebService(id);
                            objects.remove(position);
                            notifyDataSetChanged();
                            Toast.makeText(getApplicationContext(), "Contacto eliminado con exito", Toast.LENGTH_SHORT).show();

                        }
                    });

            alertDialogBuilder.setNegativeButton("Cancel",
                    new DialogInterface.OnClickListener()
                    {

                        @Override
                        public void onClick(DialogInterface arg0, int arg1)
                        {
                            Toast.makeText(getApplicationContext(),"bay", Toast.LENGTH_SHORT).show();
                        }
                    });

            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }
    }


}
